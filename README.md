#MGS-ContextMenu

##概述
Unity3D 制作UGUI上下文菜单 插件包。

##需求
Unity3D 场景中，鼠标右键点击目标物体时弹出上下文菜单，点击菜单项时对目标物体执行相应的操作。

##实现
- ContextMenuUI.cs：管理上下文菜单UI。
- ContextMenuTrigger.cs：上下文菜单触发器，鼠标右键单击物体时触发菜单显示。
- ContextMenuAgent.cs：上下文菜单代理，实现菜单项被点击时需要执行的操作。

实际上是搭建了一个右键上下文菜单的简易框架，读者需要自己编写代码组件，通过继承ContextMenuAgent类并具体实现其OnMenuItemClick方法来实现菜单项被点击时需要执行的操作，并将代码组件挂载到目标物体。例如演示案例中的TransformExample组件，ColorExample组件。

##案例
“MGS-ContextMenu\Scenes”目录下存放上述功能的演示案例，供读者参考。
