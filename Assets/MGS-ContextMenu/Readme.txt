==========================================================================
  Copyright (C), 2017-2018, Mogoson tech. Co., Ltd.
  Name: MGS-ContextMenu
  Author: Mogoson   Version: 1.0   Date: 6/16/2017
==========================================================================
  [Summeray]
    This package can be used to make context menu UI in Unity3D scene.
--------------------------------------------------------------------------
  [Environment]
    Package applies to Unity3D 5.0, .Net Framework 3.0 or above version.
--------------------------------------------------------------------------
  [Usage]
    Find the demos in the path "MGS-ContextMenu/Scenes".
    Understand the usages of component scripts in the demos.
    Use the compnent scripts in your project.
    Override the function OnMenuItemClick of ContextMenuAgent class to
    do some thing you want, just like TransformExample and ColorExample.
--------------------------------------------------------------------------
  [Contact]
    If you have any questions, feel free to contact me at mogoson@qq.com.
--------------------------------------------------------------------------